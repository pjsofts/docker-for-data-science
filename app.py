import os
import psycopg2
from flask import Flask, request
from sklearn.datasets import load_iris

from sklearn.model_selection import train_test_split

from sklearn.linear_model import LogisticRegression

from sklearn.metrics import accuracy_score


app = Flask(__name__)

conn = psycopg2.connect(
        host=os.environ['DB_HOST'],
        database="predictions",
        user='postgres',
        password='mysecretpassword')


# conn.close()






# Load the iris dataset

iris = load_iris()
X = iris.data
y = iris.target

# Train a logistic regression model
clf = LogisticRegression()
clf.fit(X, y)


# Make predictions




# Print the accuracy of the model

# accuracy_score = accuracy_score(y_test, y_pred)

# print(f'Accuracy: {accuracy_score}')





@app.route("/")
def hello_world():
    return "<h1>Hello, World!</h1>"


@app.route("/predict")
def predict():
    # Open a cursor to perform database operations
    cur = conn.cursor()

    x1 = float(request.args.get('x1'))
    x2 = float(request.args.get('x2'))
    s1 = float(request.args.get('s1'))
    s2 = float(request.args.get('s2'))
    my_input = [[x1, x2, s1, s2 ]]
    
    # Insert data into the table
    y_pred = clf.predict(my_input)

    print("prediction is ", y_pred)

    cur.execute('INSERT INTO requests (x1, x2, s1, s2, t1)'
                'VALUES (%s, %s, %s, %s, %s)',
                (str(x1),
                str(x2),
                str(s1),
                str(s2),
                str(y_pred[0]))
                )

    conn.commit()

    cur.close()
    return "<h1>Prediction %s</h1>"%(y_pred[0]) 